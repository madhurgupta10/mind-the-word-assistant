### Screenshot
![Screenshot](https://files.gitter.im/madhurgupta10/OhME/screenshot-console.actions.google.com-2019.01.19-20-20-24.png)

### Deploying Guide

The deployment of the Google Assistant Action is done in two parts: Deployment of DialogFlow Agent and Deployment of Firebase Cloud Function.

#### Deploying DialogFlow Agent:

- Zip the files together [agent.json](agent.json), [package.json](package.json) and folder [intents](intents/)
- Once you have a zip, head to [Actions on Google](https://console.actions.google.com/u/0/) and create a new project by entering a name and country for the project
- Once the project is created, you will be on the Onboarding screen. Select 'Education & Reference' on this screen.
- Fill in the basic details about your action such as invocation phrase, voice etc in the 'Quick Setup' section.
- In 'Build your Action' section, select 'Add new action' and select 'Custom Intent' in the consecutive dialog box and click 'Build'
- You will redirected to DialogFlow's project page. On this screen - select your timezone - don't change the language. Then select 'Create' and wait for the process to complete.
- Once done, select the Settings gear icon at the top left and then go to Export and Import tab.
- On this screen, select 'Restore from Zip' and restore the zip you created in the first step!
- Go to the 'Fulfillment' tab and you will see a field 'Inline-Editor', enable that.
- Paste all the code from [index.js](Firebase-Function/index.js) and [package.json](Firebase-Function/package.json) in their respective editors.
- Click on Deploy and You are done!
- Now, the Google assistant action has been deployed only for your Gmail account. You can fire up your Google Assistant app and test it!


License
----
See [LICENSE](LICENSE).